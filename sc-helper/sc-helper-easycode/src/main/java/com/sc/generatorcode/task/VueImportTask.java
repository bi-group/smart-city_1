package com.sc.generatorcode.task;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：wust
 * @date：2020-06-30
 */
public class VueImportTask extends AbstractTask {
    public VueImportTask(String className, List<ColumnInfo> infos) {
        super(className,infos);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String postfixName =  super.getName()[0];
        String name = super.getName()[1];



        Map<String, String> data = new HashMap<>();
        data.put("Author", ConfigUtil.getConfiguration().getAuthor());
        data.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        data.put("moduleName", postfixName);
        data.put("name", name);
        data.put("pageName", postfixName + "Import");
        data.put("axiosReqPrefix", ConfigUtil.getConfiguration().getAxiosReqPrefix());
        data.put("xmlName",  ConfigUtil.getConfiguration().getServerName().concat("-").concat(name).replaceAll("-","_"));

        String fileName = name + "-import.vue";
        String filePath = StringUtil.package2Path(ConfigUtil.getConfiguration().getPath().getVueDir());
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_VUE_IMPORT, data, filePath + File.separator + name,fileName);
    }
}
