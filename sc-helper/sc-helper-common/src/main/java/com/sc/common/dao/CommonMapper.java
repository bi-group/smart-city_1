package com.sc.common.dao;

import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

public interface CommonMapper {
    List<Map<String, Object>> findBySql(Map<String, Object> parameters) throws DataAccessException;

    int createDataBase(Map parameters) throws DataAccessException;

    int createDataBaseUser(Map parameters) throws DataAccessException;

    int rollbackDataBase(Map parameters) throws DataAccessException;
}
