package com.sc.common.entity.admin.project;

/**
 * @author ：wust
 * @date ：Created in 2019/7/30 11:41
 * @description：
 * @version:
 */
public class SysProjectList extends SysProject {
    private static final long serialVersionUID = 4236885841193424984L;

    private String agentName;
    private String parentCompanyName;
    private String branchCompanyName;

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getParentCompanyName() {
        return parentCompanyName;
    }

    public void setParentCompanyName(String parentCompanyName) {
        this.parentCompanyName = parentCompanyName;
    }

    public String getBranchCompanyName() {
        return branchCompanyName;
    }

    public void setBranchCompanyName(String branchCompanyName) {
        this.branchCompanyName = branchCompanyName;
    }
}
