package com.sc.demo2.core.service;

import com.sc.common.service.BaseService;
import com.sc.demo2.entity.test.Test2;

/**
 * @author: wust
 * @date: 2020-07-04 13:29:41
 * @description:
 */
public interface TestService extends BaseService<Test2>{
}
