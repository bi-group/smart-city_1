package com.sc.demo2.common.util;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.lookup.SysLookup;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * @author ：wust
 * @date ：Created in 2019/9/25 15:01
 * @description：数据字典转换工具，主要用于旧版本的数据转换到新版本的数据字典
 * @version:
 */
public class ConvertDataDictionaryUtil {
    static Logger logger = LogManager.getLogger(ConvertDataDictionaryUtil.class);

    private ConvertDataDictionaryUtil(){}

    public static String convertBreakerModel(String model){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        List<SysLookup> lookupList =  DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),"B1018");
        if(CollectionUtil.isNotEmpty(lookupList)){
            for (SysLookup sysLookup : lookupList) {
                if(model.equals(sysLookup.getValue())){
                    return sysLookup.getCode();
                }
            }
        }
        return "";
    }
}
