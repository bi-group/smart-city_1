package com.sc.order.common.config;


import com.sc.ds.config.DruidDataSourceConfigurationDefault;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MybatisAutoConfig extends DruidDataSourceConfigurationDefault {
}
