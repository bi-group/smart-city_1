package com.sc.order.core.service;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-07-15 13:54:06
 * @description:
 */
public interface GoodsImportService {
    WebResponseDto importByExcel(JSONObject jsonObject);
}
