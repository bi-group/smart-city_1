package com.sc.order.core.service.impl;

import com.sc.order.core.dao.OrderDetailMapper;
import com.sc.order.core.service.OrderDetailService;
import com.sc.order.entity.detail.OrderDetail;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import com.sc.common.dto.WebResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: 2020-07-15 14:16:52
 * @description:
 */
@Service("orderDetailServiceImpl")
public class OrderDetailServiceImpl extends BaseServiceImpl<OrderDetail> implements OrderDetailService{
    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return orderDetailMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
