package com.sc.order.entity.detail;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-07-15 14:16:53
 * @description:
 */
public class OrderDetailSearch extends OrderDetail {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}