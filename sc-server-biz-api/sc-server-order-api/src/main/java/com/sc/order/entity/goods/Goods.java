package com.sc.order.entity.goods;

import com.sc.common.entity.BaseEntity;

import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author: wust
 * @date: 2020-07-15 13:54:06
 * @description:
 */
@Table(name = "t_goods")
public class Goods extends BaseEntity {
        private String goodsNo;
    private String name;
    private BigDecimal unitPrice;
    private Long picture;
    private Long stock;
    private String description;
    private Long projectId;
    private Long companyId;
                            private Integer isDeleted;


        public void setGoodsNo (String goodsNo) {this.goodsNo = goodsNo;} 
    public String getGoodsNo(){ return goodsNo;} 
    public void setName (String name) {this.name = name;} 
    public String getName(){ return name;} 
    public void setUnitPrice (BigDecimal unitPrice) {this.unitPrice = unitPrice;} 
    public BigDecimal getUnitPrice(){ return unitPrice;} 
    public void setPicture (Long picture) {this.picture = picture;} 
    public Long getPicture(){ return picture;} 
    public void setStock (Long stock) {this.stock = stock;} 
    public Long getStock(){ return stock;} 
    public void setDescription (String description) {this.description = description;} 
    public String getDescription(){ return description;} 
    public void setProjectId (Long projectId) {this.projectId = projectId;} 
    public Long getProjectId(){ return projectId;} 
    public void setCompanyId (Long companyId) {this.companyId = companyId;} 
    public Long getCompanyId(){ return companyId;} 
                            public void setIsDeleted (Integer isDeleted) {this.isDeleted = isDeleted;} 
    public Integer getIsDeleted(){ return isDeleted;} 

}