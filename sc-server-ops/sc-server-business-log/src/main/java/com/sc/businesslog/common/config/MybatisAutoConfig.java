package com.sc.businesslog.common.config;


import com.sc.ds.config.ShardingsphereDataSourceConfigurationDefault;
import org.springframework.context.annotation.Configuration;

/**
 * Created by wust on 2019/6/20.
 */
@Configuration
public class MybatisAutoConfig extends ShardingsphereDataSourceConfigurationDefault {
}
