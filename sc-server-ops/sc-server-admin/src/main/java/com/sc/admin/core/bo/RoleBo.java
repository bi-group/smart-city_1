package com.sc.admin.core.bo;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.admin.core.dao.SysRoleMapper;
import com.sc.common.entity.admin.role.SysRole;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.CodeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class RoleBo {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * 根据部门code判断部门是否已经存在
     * @param code
     * @return
     */
    public boolean isExistsByCode(String code){
        SysRole roleSearch = new SysRole();
        roleSearch.setCode(code);
        List<SysRole> roles = sysRoleMapper.select(roleSearch);
        if(CollectionUtil.isNotEmpty(roles)){
            return true;
        }
        return false;
    }


    public String getCode(){
        long start = System.currentTimeMillis();
        String code = CodeGenerator.genRoleCode();
        while (isExistsByCode(code)){
            code = CodeGenerator.genRoleCode();
            long end = System.currentTimeMillis();
            if((end - start) / 1000 > 20){ // 20秒超时设置，防止活锁
                throw new BusinessException("生成部门编码失败");
            }
        }
        return code;
    }
}
