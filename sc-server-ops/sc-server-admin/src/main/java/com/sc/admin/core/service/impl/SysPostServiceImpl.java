package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysPostMapper;
import com.sc.admin.core.service.SysPostService;
import com.sc.admin.entity.post.SysPost;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import com.sc.common.dto.WebResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: 2020-12-03 11:14:47
 * @description:
 */
@Service("sysPostServiceImpl")
public class SysPostServiceImpl extends BaseServiceImpl<SysPost> implements SysPostService{
    @Autowired
    private SysPostMapper sysPostMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysPostMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
