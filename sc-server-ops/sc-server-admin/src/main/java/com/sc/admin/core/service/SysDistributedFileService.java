package com.sc.admin.core.service;

import com.sc.common.service.BaseService;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;

/**
 * @author: wust
 * @date: 2020-03-08 15:25:30
 * @description:
 */
public interface SysDistributedFileService extends BaseService<SysDistributedFile>{
}
